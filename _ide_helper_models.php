<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Entity{
/**
 * App\Entity\Comments
 *
 * @property-read \App\Entity\Post $post
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Comments newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Comments newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Comments query()
 */
	class Comments extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Post
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Comments[] $comments
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Post query()
 */
	class Post extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 */
	class User extends \Eloquent {}
}

