<?php


namespace App\Service\Api;


use App\Entity\Post;

class PostService
{
	public function commentService(Post $post)
	{
		$comms = [];
		foreach ($post->comments()->get() as $comment) {
			if ( $post->id === $comment->post_id ) {
				$comms[]= [
					'id'   => $comment->id,
					'text' => $comment->comments,
					'user' => $comment->user()->get()
				];
				$post->comments = $comms;
			}
		}
		return $post;
	}
}