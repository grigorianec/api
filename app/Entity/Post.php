<?php


namespace App\Entity;


use App\Models\Post as PostInterface;
use App\User;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements PostInterface
{
	protected $table = 'post';

	protected $fillable = ['title','user_id' ,'description'];

	public function comments()
	{
		return $this->hasMany(Comments::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * Generate $number entries of this type
	 *
	 * @param int $number
	 */
	public function generate( int $number ): void {
		// TODO: Implement generate() method.
	}

	/**
	 * @param int $options
	 *
	 * @return string JSON representation of the model's public fields
	 */
	public function toJSON($options = 0): string {
		$json = json_encode($this->jsonSerialize(), $options);

		if (JSON_ERROR_NONE !== json_last_error()) {
			throw JsonEncodingException::forModel($this, json_last_error_msg());
		}

		return $json;
	}


}