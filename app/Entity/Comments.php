<?php


namespace App\Entity;


use App\Models\Comment;
use App\User;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model implements Comment
{

	protected $fillable = [
		'post_id','user_id','comments'
	];


	public function post()
	{
		return $this->belongsTo(Post::class);
	}
	Public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * Generate $number entries of this type
	 *
	 * @param int $number
	 */
	function generate( int $number ): void {
		// TODO: Implement generate() method.
	}

	/**
	 * @param int $options
	 *
	 * @return string JSON representation of the model's public fields
	 */
	public function toJSON($options = 0): string {
		$json = json_encode($this->jsonSerialize(), $options);

		if (JSON_ERROR_NONE !== json_last_error()) {
			throw JsonEncodingException::forModel($this, json_last_error_msg());
		}

		return $json;
	}
}