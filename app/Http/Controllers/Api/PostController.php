<?php


namespace App\Http\Controllers\Api;


use App\Entity\Post;
use App\Http\Controllers\Controller;
use App\Service\Api\PostService;
use Illuminate\Http\Request;


class PostController extends Controller
{
	/**
	 * @var PostService
	 */
	private $postService;

	public function __construct(PostService $postService)
	{

		$this->postService = $postService;
	}

	public function home()
	{
		return view('post.home');
	}
	public function index(Request $request)
	{
		if (empty($request)){
			$posts = Post::get(['id', 'created_at', 'views', 'description']);
		}else{
			$sort = $request->get('sort');
			if ($request->get('sortType') == 'asc'){
				$sortType = 1;
			}else{
				$sortType = 2;
			}
			$page = $request->get('page');
			$perPage = $request->get('perPage');
			$posts = Post::get(['id', 'created_at', 'views', 'description'])
			             ->sortBy($sort, $sortType)
			             ->forPage($page,$perPage);
		}

		foreach ($posts as $post){
			$this->postService->commentService($post);
		}
		return response()->json([
			$posts,
		]);
	}

	public function show(Post $post){
		$post->views++;
		$post->save();
		$this->postService->commentService($post);

		return response()->json([
			'post' => $post,
		]);
	}

	public function store(Request $request)
	{
		$this->validate(request(),[
			'title'       => 'required',
			'description'     => 'required|min:10',
		]);
		Post::create([
			'title'       => request('title'),
			'user_id'     => $request->user()->id(),
//			'user_id'     => 1,
			'description'     => request('description'),
		]);
		return response()->json([
			'success' => 'You added post',
		]);
	}

	public function update(Post $post)
	{
		$this->validate(request(),[
			'title'       => 'required',
			'description'     => 'required|min:10',
		]);
		$post->update([
			'title' => request('title'),
			'description' => request('description'),
		]);
		return response()->json([
			'success' => 'The post was updated',
		]);
	}
	public function destroy(Post $post)
	{
		$post->delete();
		return response()->json([
			'success' => 'The post was deleted',
		]);
	}
}