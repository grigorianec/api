<?php


namespace App\Http\Controllers\Posts;


use App\Entity\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
	public function home()
	{
		return view('post.home');
	}
	public function index(Request $request)
	{
		if (empty($request)){
			$posts = Post::get(['id', 'created_at', 'views', 'description']);
		}else{
			$sort = $request->get('sort');
			if ($request->get('sortType') == 'asc'){
				$sortType = 1;
			}else{
				$sortType = 2;
			}
			$page = $request->get('page');
			$perPage = $request->get('perPage');
			$posts = Post::get(['id', 'created_at', 'views', 'description'])
			             ->sortBy($sort, $sortType)
			             ->forPage($page,$perPage);
		}

		return view('post.index',compact('posts'));
	}
	public function create()
	{
		return view('post.create');
	}
	public function store()
	{
		$this->validate(request(),[
			'title'       => 'required',
			'description'     => 'required|min:10',
		]);
		Post::create([
			'title'       => request('title'),
			'user_id'     =>auth()->id(),
			'description'     => request('description'),
		]);
		return redirect()->route('post.index')->with('success','Post successfully added');
	}

	public function edit(Post $post)
	{
		return view('post.edit',compact('post'));
	}
	public function update(Post $post)
	{
		$this->validate(request(),[
			'title'       => 'required',
			'description'     => 'required|min:10',
		]);
		$post->update([
			'title' => request('title'),
			'description' => request('description'),
		]);
		return redirect()->route('post.index')->with('info','Post successfully change');
	}
	public function destroy(Post $post)
	{
		$post->delete();
		return redirect()->route('post.index')->with('danger','Post successfully remove');
	}

	public function show(Post $post){
		$post->views++;
		$post->save();

		return view('post.show',compact('post'));
	}
}