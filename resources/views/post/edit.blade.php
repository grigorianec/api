@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-description-center">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header">Edit Post</div>
                    <div class="card-body">
                        <form action="{{ route('post.update', $post) }}" method="post">
                            @csrf
                            @method('PUt')
                            <div class="form-group has-feedback{{ $errors->has('title') ? ' has-error' :'' }}">
                                <label for="">Title</label>
                                <input type="text" class="form-control" value="{{ $post->title }}" name="title" placeholder="Post Title" value="{{ old('title') }}">
                                @if ($errors->has('title'))
                                    <span class="help-block"><p>{{ $errors->first('title') }}</p></span>
                                @endif
                            </div>
                            <div class="form-group has-feedback{{ $errors->has('description') ? ' has-error' :'' }}">
                                <label for="description">Description</label>
                                <textarea id="description" name="description" rows="5" class="form-control" placeholder="Post description">{{ $post->description }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                <p>{{ $errors->first('description') }}</p>
                             </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class=" btn btn-primary">
                                    Save
                                </button>
                                <a class="btn btn-primary" href="{{ route('post.index') }}">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection