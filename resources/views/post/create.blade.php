
@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('post.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback{{ $errors->has('title') ? ' has-error' :'' }}">
                <label for="">Title</label>
                <input type="text" class="form-control" name="title" placeholder="Post Title" value="{{ old('title') }}">
                @if ($errors->has('title'))
                    <span class="help-block">
          <p>{{ $errors->first('title') }}</p>
        </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('description') ? ' has-error' :'' }}">
                <label for="description">Content</label>
                <textarea id="description" name="content" rows="5" class="form-control" placeholder="Post Content">{{ old('description') }}</textarea>
                @if ($errors->has('description'))
                    <span class="help-block">
          <p>{{ $errors->first('description') }}</p>
        </span>
                @endif
            </div>
            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection