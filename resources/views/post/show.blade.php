@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-md-offset-2">

                <!-- Box Detail Post -->
                <div class="card">
                    <div class="card-header">
                        <small> {{ $post->title }}</small>
                        <div class="btn-group float-right" role="group">
                            <a class="btn btn-primary" href="{{ route('post.index') }}">Back</a>
                            <form action="{{ route('post.edit', $post) }}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-sm btn-success">
                                    Edit
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>{!! $post->description !!}</p><br>
                        <div>
                            Просмотров {{ $post->views }}
                        </div>
                    </div>

                </div><br/>
{{--                {{ dump($post) }}--}}
                <!-- Box for Show Comments -->
                    @foreach ($post->comments()->get() as $comment)
                        <div class="card">
                            <div class="card-header">
                                {{ $comment->user->name }} - {{ $comment->created_at }}
                                @if ($comment->user_id == auth()->id())
                                    <div class="btn-group float-right" role="group">
                                        <form action="" id="btn-comment-delete" onsubmit="return comment_destrox()">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_id" value="{{ $comment->id }}">
                                            <button type="submit" class="btn btn-sm btn-danger">Remove</button>
                                        </form>
                                    </div>
                                    <div class="btn-group mr-2 float-right" role="group">
                                        <form action="{{ route('post.comment.edit',[$post,$comment]) }}" method="post">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-sm btn-success">Edit</button>
                                        </form>
                                    </div>
                                @endif
                            </div>
                            <div class="card-body">
                                {!! $comment->comments !!}
                            </div>
                        </div>
                        <br/>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection