<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
	'namespace' => 'Posts',
	'middleware' => ['auth']
	],
	function(){
	Route::get('/post/home', 'PostController@home')->name('post.home');
	Route::get('/posts', 'PostController@index')->name('post.index');
	Route::get('/post/{post}', 'PostController@show')->name('post.show');

		Route::get('/post/create', 'PostController@create')->name('post.create');
		Route::post('/post/create', 'PostController@store')->name('post.store');
		Route::post('/post/{post}/edit', 'PostController@edit')->name('post.edit');
		Route::put('/post/{post}/edit', 'PostController@update')->name('post.update');

		Route::delete('/post/{post}/delete', 'PostController@destroy')->name('post.destroy');

		Route::post('/post/{post}/comment', 'PostCommentController@store')->name('post.comment.store');

		Route::post('/post/{post}/comment/{comment}/edit', 'PostCommentController@edit')->name('post.comment.edit');
		Route::get('/post/{post}/comment/{comment}/edit', 'PostCommentController@edit')->name('post.comment.edit');
		Route::patch('/post/{post}/comment/{comment}/edit', 'PostCommentController@update')->name('post.comment.update');
		Route::delete('/post/{post}/{comment}/delete', 'PostCommentController@destroy')->name('post.comment.destroy');
		Route::get('/post/comment/delete', 'PostCommentController@destrox')->name('post.comment.destrox');

});

