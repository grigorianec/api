<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::post('/register', 'Auth\RegisterController@register');
//Route::get('/posts', 'Api\PostController@index');
//Route::get('/post/{post}', 'Api\PostController@show');
//Route::post('/post/create', 'Api\PostController@store');
//Route::patch('/post/{post}/edit', 'Api\PostController@update');
//Route::delete('/post/{post}/delete', 'Api\PostController@destroy');

Route::group(['as' => 'api.', 'namespace' => 'Api', 'middleware' => ['auth:api']],
	function () {
		Route::get('/', 'HomeController@home');
		Route::get('/posts', 'PostController@index');
		Route::get('/post/{post}', 'PostController@show');
		Route::post('/post/create', 'PostController@store');
		Route::patch('/post/{post}/edit', 'PostController@update');
		Route::delete('/post/{post}/delete', 'PostController@destroy');
	});
