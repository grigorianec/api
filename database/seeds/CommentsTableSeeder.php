<?php


use App\Entity\Comments;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
	public function run(): void
	{
		factory( Comments::class, 30)->create();
	}
}