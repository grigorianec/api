<?php

use App\Entity\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
	public function run(): void
	{
		factory(Post::class, 20)->create();
	}
}
