<?php
/** @var Factory $factory */
use App\Entity\Post;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


$factory->define(Post::class, function (Faker $faker) {
	return [
		'user_id' => rand(1,10),
		'title' => $faker->title,
		'description' => $faker->realText(),
	];
});
