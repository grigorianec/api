<?php
/** @var Factory $factory */

use App\Entity\Comments;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;

$factory->define( Comments::class, function (Faker $faker) {
	return [
		'post_id' => App\Entity\Post::inRandomOrder()->value('id'),
		'user_id' => App\Entity\Comments::inRandomOrder()->value('id'),
		'comments' => $faker->text,
	];
});
